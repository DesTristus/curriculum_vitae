%-----------------------------------------------------------------------------------------------------------------------------------------------%
%	The MIT License (MIT)
%
%	Copyright (c) 2015 Jan Küster
%
%	Permission is hereby granted, free of charge, to any person obtaining a copy
%	of this software and associated documentation files (the "Software"), to deal
%	in the Software without restriction, including without limitation the rights
%	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%	copies of the Software, and to permit persons to whom the Software is
%	furnished to do so, subject to the following conditions:
%	
%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%	THE SOFTWARE.
%	
%
%-----------------------------------------------------------------------------------------------------------------------------------------------%


%============================================================================%
%
%	DOCUMENT DEFINITION
%
%============================================================================%

%we use article class because we want to fully customize the page and dont use a cv template
\documentclass[10pt,A4]{article}	


%----------------------------------------------------------------------------------------
%	ENCODING
%----------------------------------------------------------------------------------------

%we use utf8 since we want to build from any machine
\usepackage[utf8]{inputenc}		

%----------------------------------------------------------------------------------------
%	LOGIC
%----------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{xifthen}

%----------------------------------------------------------------------------------------
%	FONT
%----------------------------------------------------------------------------------------

% some tex-live fonts - choose your own

%\usepackage[defaultsans]{droidsans}
%\usepackage[default]{comfortaa}
%\usepackage{cmbright}
\usepackage[default]{raleway}
%\usepackage{fetamont}
%\usepackage[default]{gillius}
%\usepackage[light,math]{iwona}
%\usepackage[thin]{roboto} 

% set font default
\renewcommand*\familydefault{\sfdefault} 	
\usepackage[T1]{fontenc}

% more font size definitions
\usepackage{moresize}		
\usepackage[colorlinks = true,
            linkcolor = blue,
            urlcolor  = blue,
            citecolor = blue,
            anchorcolor = blue]{hyperref}

%----------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%----------------------------------------------------------------------------------------

%debug page outer frames
%\usepackage{showframe}			


%define page styles using geometry
\usepackage[a4paper]{geometry}		

% for example, change the margins to 2 inches all round
\geometry{top=1.75cm, bottom=-.6cm, left=1.5cm, right=1.5cm} 	

%use customized header
\usepackage{fancyhdr}				
\pagestyle{fancy}

%less space between header and content
\setlength{\headheight}{-5pt}		





%indentation is zero
\setlength{\parindent}{0mm}

%----------------------------------------------------------------------------------------
%	TABLE /ARRAY DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for layouting tables
\usepackage{multicol}			
\usepackage{multirow}

%extended aligning of tabular cells
\usepackage{array}

\newcolumntype{x}[1]{%
>{\raggedleft\hspace{0pt}}p{#1}}%


%----------------------------------------------------------------------------------------
%	GRAPHICS DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for header image
\usepackage{graphicx}

%for floating figures
\usepackage{wrapfig}
\usepackage{float}
%\floatstyle{boxed} 
%\restylefloat{figure}

%for drawing graphics		
\usepackage{tikz}				
\usetikzlibrary{shapes, backgrounds,mindmap, trees}


%----------------------------------------------------------------------------------------
%	Color DEFINITIONS
%---------------------------------------------------------------------------------------- 

\usepackage{color}

%accent color
\definecolor{sectcol}{RGB}{255,150,0}

%dark background color
\definecolor{bgcol}{RGB}{110,110,110}

%light background / accent color
\definecolor{softcol}{RGB}{225,225,225}


%============================================================================%
%
%
%	DEFINITIONS
%
%
%============================================================================%

%----------------------------------------------------------------------------------------
% 	HEADER
%----------------------------------------------------------------------------------------

% remove top header line
\renewcommand{\headrulewidth}{0pt} 

%remove botttom header line
\renewcommand{\footrulewidth}{0pt}	  	

%remove pagenum
\renewcommand{\thepage}{}	

%remove section num		
\renewcommand{\thesection}{}			

%----------------------------------------------------------------------------------------
% 	ARROW GRAPHICS in Tikz
%----------------------------------------------------------------------------------------

% a six pointed arrow poiting to the left
\newcommand{\tzlarrow}{(0,0) -- (0.2,0) -- (0.3,0.2) -- (0.2,0.4) -- (0,0.4) -- (0.1,0.2) -- cycle;}	

% include the left arrow into a tikz picture
% param1: fill color
%
\newcommand{\larrow}[1]
{\begin{tikzpicture}[scale=0.58]
	 \filldraw[fill=#1!100,draw=#1!100!black]  \tzlarrow
 \end{tikzpicture}
}

% a six pointed arrow poiting to the right
\newcommand{\tzrarrow}{ (0,0.2) -- (0.1,0) -- (0.3,0) -- (0.2,0.2) -- (0.3,0.4) -- (0.1,0.4) -- cycle;}

% include the right arrow into a tikz picture
% param1: fill color
%
\newcommand{\rarrow}
{\begin{tikzpicture}[scale=0.7]
	\filldraw[fill=sectcol!100,draw=sectcol!100!black] \tzrarrow
 \end{tikzpicture}
}



%----------------------------------------------------------------------------------------
%	custom sections
%----------------------------------------------------------------------------------------

% create a coloured box with arrow and title as cv section headline
% param 1: section title
%
\newcommand{\cvsection}[1]
{
\colorbox{sectcol}{\mystrut \makebox[1\linewidth][l]{
 \textcolor{white}{\textbf{#1}}\hspace{4pt}
}}\\
}

%create a coloured arrow with title as cv meta section section
% param 1: meta section title
%
\newcommand{\metasection}[2]
{
\begin{tabular*}{1\textwidth}{p{2.4cm} p{11cm}}
\larrow{bgcol}	\normalsize{\textcolor{sectcol}{#1}}&#2\\[5pt]
\end{tabular*}
}

%----------------------------------------------------------------------------------------
%	 CV EVENT
%----------------------------------------------------------------------------------------

% creates a stretched box as cv entry headline followed by two paragraphs about 
% the work you did
% param 1:	event time i.e. 2014 or 2011-2014 etc.
% param 2:	event name (what did you do?)
% param 3:	institution (where did you work / study)
% param 4:	what was your position
% param 5:	some words about your contributions
%
\newcommand{\cvevent}[5]
{
\vspace{8pt}
	\begin{tabular*}{1\textwidth}{p{3.4cm}  p{12.8cm}}
 \textcolor{bgcol}{#1}&{#2} 

	\end{tabular*}

\begin{tabular*}{1\textwidth}{p{2.3cm} p{14.4cm}}
\end{tabular*}
\vspace{-12pt}
\textcolor{softcol}{\hrule}
%\vspace{6pt}
%	\begin{tabular*}{1\textwidth}{p{2.3cm} p{14.4cm}}
%&		 \larrow{bgcol}  #4\\[3pt]
%&		 \larrow{bgcol}  #5\\[6pt]
%	\end{tabular*}

}

% creates a stretched box as 
\newcommand{\cveventmeta}[2]
{
	\mbox{\mystrut \hspace{87pt}\textit{#1}}\\
	#2
}

%----------------------------------------------------------------------------------------
% CUSTOM STRUT FOR EMPTY BOXES
%----------------------------------------- -----------------------------------------------
\newcommand{\mystrut}{\rule[-.3\baselineskip]{0pt}{\baselineskip}}

%----------------------------------------------------------------------------------------
% CUSTOM LOREM IPSUM
%----------------------------------------------------------------------------------------
\newcommand{\lorem}
{Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus.}



%============================================================================%
%
%
%
%	DOCUMENT CONTENT
%
%
%
%============================================================================%
\begin{document}


%use our custom fancy header definitions
\pagestyle{fancy}	


%---------------------------------------------------------------------------------------
%	TITLE HEADLINE
%----------------------------------------------------------------------------------------
\vspace{-20.55pt}

% use this for multiple words like working titles etc.
%\hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\hspace{46pt}\HUGE{\textcolor{white}{\textsc{Jan Küster}} } \textcolor{sectcol}{\rule[-1mm]{1mm}{0.9cm}} \parbox[b]{5cm}{   \large{ \textcolor{white}{{IT Consultant}}}\\
% \large{ \textcolor{white}{{Resume}}}}
%}}

% use this for single words, e.g. CV or RESUME etc.
\hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\HUGE{\textcolor{white}{\textsc{Tristan Dubos}} } \textcolor{sectcol}{\rule[-1mm]{1mm}{0.9cm}} \HUGE{\textcolor{white}{\textsc{CV}} } }}


%----------------------------------------------------------------------------------------
%	HEADER IMAGE
%----------------------------------------------------------------------------------------

\begin{figure}[H]
\begin{flushright}
	\includegraphics[width=0.2\linewidth]{photo_CV.jpg}	%trimming relative to image size!
\end{flushright}
\end{figure}



%---------------------------------------------------------------------------------------
%	META SECTION
%----------------------------------------------------------------------------------------

\vspace{-114pt}


\metasection{Age}{35 ans}
\metasection{Nationalité}{Française - Suisse}
\metasection{Adresse:}{Combaurie, 12720 Mostuéjouls France }
\metasection{Téléphone}{+33 6 79 33 33 04}
\metasection{E-mail}{\href{tristan.duos33@gmail.com}{tristan.duos33@gmail.com}}
\metasection{git}{\url{https://gitlab.com/DesTristus}}
\metasection{linkedin}{\url{https://www.linkedin.com/in/tristan-dubos-a79635b8/}}
\metasection{ORCID}{\url{https://orcid.org/0000-0002-4265-2379}}

\vspace{20pt}






%============================================================================%
%
%	CV SECTIONS AND EVENTS (MAIN CONTENT)
%
%============================================================================%

%---------------------------------------------------------------------------------------
%	EXPERIENCE
%----------------------------------------------------------------------------------------
\cvsection{Expériences professionnelles}

%


%\textcolor{softcol}{\hrule}

\cvevent{03/2022 - 09/2023}{\textbf{Post doctorat projet Deep4Mix (INRAE)} : segmentation d'espèces en contexte de cultures mixtes. Exploration de méthodes pour \textbf{automatiser} la surveillance de la croissance de plantes en contextes de polyculture. \textbf{Entraînement de modèles d'apprentissages profonds  (U-Net et ResNet)} pour la \textbf{segmentation sémantique multi-classes} à partir d'images RGB. 

\textbf{Contact}: Chef d'équipe Marie Weiss : \href{marie.weiss@inrae.fr
}{marie.weiss@inrae.fr}

\textbf{Contact}: Ingénieur de recherche Sylvain Jay : \href{sylvain.jay@inrae.fr
}{sylvain.jay@inrae.fr}
 
}

%


%\textcolor{softcol}{\hrule}

\cvevent{04/2018 - 09/2021}{\textbf{Doctorat en informatique spécialité bio-informatique}: Optimisation et automatisation d'outils de bio-imagerie 3D pour caractériser la morphologie nucléaire et l'organisation de la chromatine. \textbf{Développement de plugins imageJ} (\href{https://gitlab.com/DesTristus/NucleusJ2.0}{NucleusJ2.0}) et de \textbf{modèles de machine learning}, pour automatiser la détection et la caractérisation de l’organisation 3D des noyaux de cellules chez A. \textit{thaliana} (GReD/Institut Pascal Clermont-Ferrand).

\textbf{Contact}: Co-directrice de thèse Sophie Desset: \href{sophie.desset@uca.fr}{sophie.desset@uca.fr}


\textbf{Contact}: Chef d'équipe Christophe Tatout :\href{mailto:christophe.tatout@uca.fr}{christophe.tatout@uca.fr}}

%\textcolor{softcol}{\hrule}


\cvevent{04/2018 - 09/2021}{\textbf{Activité d'enseignement en bio-informatique 160 heures}: IUT 2ème année en bio-informatique et licence 3 de biologie \textbf{100h}: annotation structurale et fonctionnelle de séquences d'ADN (ORFFinder,Blast,interproscan) et phylogénétique (Phylogeny.fr). Licence 3 de biologie \textbf{60h}: initiation à la programmation objet en biologie (language Java).

\textbf{Contact}: Responsable enseignement Gisèle Bronner: \href{gisele.bronner@uca.fr}{gisele.bronner@uca.fr}


\textbf{Contact}: Responsable enseignement Valérie Polonais: \href{valerie.polonais@uca.fr}{valerie.polonais@uca.fr}}




%
\cvevent{10/2015 - 04/2018}{\textbf{Ingénieur d’étude bio-informaticien} - équipe de génétique du CHRU-Nancy: Chargé de la mise en place de pipelines pour le \textbf{diagnostic génétique de cancérologie et maladies rares} \textbf{(SNV, CNV)}, bases de données et développement de web service pour les utilisateurs. Formation et sensibilisation aux outils bio-informatiques \textbf{NGS}. 

\textbf{Contact}: Chef d'équipe Céline Bonnet :\href{mailto:ce.bonnet@chru-nancy.fr}{ce.bonnet@chru-nancy.fr}}


%\textcolor{softcol}{\hrule}

%
\cvevent{02/2015 - 07-2015}{\textbf{Stage master 2}: Quantification, assemblage et caractérisation polymorphique de séquences répétées d’ADN à partir d'informations issues de NGS dans l'équipe GReD (CNRS-Clermont-Ferrand).

Contact: Encadrant de stage Christophe Tatout :\href{mailto:christophe.tatout@uca.fr}{christophe.tatout@uca.fr}}



%\textcolor{softcol}{\hrule}


%

%---------------------------------------------------------------------------------------
%	EDUCATION SECTION
%--------------------------------------------------------------------------------------


\cvsection{Formations académiques}
%\textcolor{softcol}{\hrule}

%
\cvevent{04/2018 - 09/2021}{\textbf{Doctorat en bio-informatique} spécialité génie logiciel à l'Université Clermont Auvergne Clermont-Ferrand.}


%\textcolor{softcol}{\hrule}


\cvevent{09/2013 - 09/2015}{\textbf{Master de Génétique et physiologie}: spécialisation en Analyse et Modélisation des Données à l'Université Clermont Auvergne Clermont-Ferrand.}


%\textcolor{softcol}{\hrule}

\cvevent{09/2012 - 07/2013}{\textbf{Licence en biologie mention BCP} (biologie cellulaire et physiologie) à l'Université Clermont Auvergne Clermont-Ferrand.}


%\textcolor{softcol}{\hrule}


\cvevent{2010}{\textbf{Brevet de technicien supérieur agricole (BTSA) en gestion protection de la nature (GPN)} option gestion des espaces naturels (candidat libre)}


%\textcolor{softcol}{\hrule}


\cvsection{Publications}



%\textcolor{softcol}{\hrule}

\cvevent{2022}{\textbf{Dubos, T.}, Poulet, A., Thomson, G. et al. \emph{\textbf{ NODeJ: an ImageJ plugin for 3D segmentation of nuclear objects.}} BMC Bioinformatics 23, 216 (2022). \url{https://doi.org/10.1186/s12859-022-04743-6}}

%\textcolor{softcol}{\hrule}


\cvevent{2022}{Sarah Mermet, Maxime Voisin, Joris Mordier, \textbf{Tristan Dubos}, Sylvie Tutois, Pierre Tuffery, Célia Baroux, Kentaro Tamura, Aline V Probst, Emmanuel Vanrobays, Christophe Tatout, \emph{\textbf{Evolutionarily conserved protein motifs drive interactions between the plant nucleoskeleton and nuclear pores}}, The Plant Cell, 2023;, koad236, \url{https://doi.org/10.1093/plcell/koad236}}


%\textcolor{softcol}{\hrule}

\cvevent{2022}{Guillaume Mougeot,\textbf{Tristan Dubos}, Frédéric Chausse, Emilie Péry, Katja Graumann, Christophe Tatout, David E. Evans, Sophie Desset; \emph{\textbf{Deep learning ­– promises for 3D nuclear imaging: a guide for biologists}}. J Cell Sci 1 April 2022; 135 (7): jcs258986. doi: \url{https://doi.org/10.1242/jcs.258986}}




%\textcolor{softcol}{\hrule}

\cvevent{2020}{\textbf{Tristan Dubos}, Axel Poulet, Céline Gonthier-Gueret, Guillaume Mougeot, Emmanuel Vanrobays, Yanru Li, Sylvie Tutois, Emilie Pery, Frédéric Chausse, Aline V. Probst, Christophe Tatout \& Sophie Desset (2020) \emph{\textbf{Automated 3D bio-imaging analysis of nuclear organization by NucleusJ 2.0}}, Nucleus, 11:1, 315-329, DOI: \url{10.1080/19491034.2020.1845012}}


%\textcolor{softcol}{\hrule}


\cvevent{2019}{Axel Poulet, Ben Li, \textbf{Tristan Dubos}, Juan Carlos Rivera-Mulia, David M Gilbert, Zhaohui S Qin, \emph{\textbf{RT States: systematic annotation of the human genome using cell type-specific replication timing programs}}, Bioinformatics, Volume 35, Issue 13, 1 July 2019, Pages 2167–2176,\url{ https://doi.org/10.1093/bioinformatics/bty957}}

%\textcolor{softcol}{\hrule}


\cvevent{2018}{Simon L, Rabanal FA, \textbf{Dubos T}, et al. \emph{\textbf{Genetic and epigenetic variation in 5S ribosomal RNA genes reveals genome dynamics in Arabidopsis thaliana.}} Nucleic Acids Research. 2018 Apr;46(6):3019-3033. \url{DOI: 10.1093/nar/gky163}}



\cvsection{Participation à des évènements scientifiques}




%\textcolor{softcol}{\hrule}


 
\cvevent{02/2020}{\textbf{Congrès NEUBIAS (Network of European BioImage Analysts) Bordeaux}: poster "Automated 3D analysis of nuclear organization in culture cells and  tissues using deep learning method".}


%\textcolor{softcol}{\hrule}

 
\cvevent{02/2019}{\textbf{Congrès NEUBIAS (Network of European BioImage Analysts) Luxembourg}: présentation de poster: "Improvement and automatization of an image analysis too to characterize the plant 3D nucleus". Orale - "Call For Help" session: "How to automatically and efficiently segment irregular and fuzzy objects ?".}


%\textcolor{softcol}{\hrule}



\cvevent{07/2015}{\textbf{Congrès JOBIM (Journées Ouvertes en Biologie, Informatique et Mathématiques)} Clermont-Ferrand : présentation de poster sur le sujet du stage de master 2: Cartographie et assemblage denovo de sequences issues de séquenceurs à haut debit couvrant des séquences répétees dans le génome d’\textit{A. thaliana}.}











\cvsection{Programmation et outils bio-informatiques}


\cvevent{Languages }{python, R, JAVA, shell(Bash), LaTeX, markdown, Sql, perl}



\cvevent{Outils }{imageJ/fiji, git, conda, Docker, cvat, jupyter-notbook, cuda, R-ggplot2, keras, tensorflow, pytorch}


%-------------------------------------------------------------------------------------------------
%	ARTIFICIAL FOOTER (fancy footer cannot exceed linewidth) 
%--------------------------------------------------------------------------------------------------

%\null
%\vspace*{\fill}
%\hspace{-0.25\linewidth}\colorbox{bgcol}{\makebox[1.5\linewidth][c]{\mystrut \small \textcolor{white}{}  \textcolor{white}{}}}




%============================================================================%
%
%
%
%	DOCUMENT END
%
%
%
%============================================================================%
\end{document}
